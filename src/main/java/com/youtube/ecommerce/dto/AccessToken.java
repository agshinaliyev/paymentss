package com.youtube.ecommerce.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccessToken {

    private String accessToken;

}
