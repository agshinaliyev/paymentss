package com.youtube.ecommerce.dto;

import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Builder
@Data
public class ErrorResponseDto {
    int status;
    public String title;
    public String details;
    @Builder.Default
    Map<String, Object> data = new HashMap<>();


}
