package com.youtube.ecommerce.dto;

import jakarta.validation.constraints.Size;
import lombok.Data;

@Data

public class UserResponse {

    private String username;
    private String mail;
    private String password;
    private String firstName;
    private String lastName;

}
