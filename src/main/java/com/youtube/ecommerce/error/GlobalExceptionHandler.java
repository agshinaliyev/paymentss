package com.youtube.ecommerce.error;

import com.youtube.ecommerce.dto.ErrorResponseDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

import static com.youtube.ecommerce.error.HttpResponseConstants.PATH;
import static com.youtube.ecommerce.error.HttpResponseConstants.STATUS;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
@Data
public class GlobalExceptionHandler extends RuntimeException {
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ErrorResponseDto> handler(ApplicationException ex, WebRequest request) {
        ex.printStackTrace();

        Map<String, Object> error = new HashMap<>();
    error.put(PATH, ((ServletWebRequest) request).getRequest().getRequestURI());

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .title("Exception occurred")
                .details(ErrorCodes.USERNAME_ALREADY_EXISTS.key)
                .status(400)
                .data(error)
                .build());
    }
//        @ExceptionHandler(HttpMessageNotReadableException.class)
//        public ResponseEntity<Map<String, Object>> handle(HttpMessageNotReadableException ex,
//                WebRequest request) {
//            log.trace("Required request body is missing {}", ex.getMessage());
//            return ofType(request, HttpStatus.BAD_REQUEST, ex);
//        }
//
////        @ExceptionHandler(ConstraintViolationException.class)
////        public final ResponseEntity<Map<String, Object>> handle(ConstraintViolationException ex,
////                WebRequest request) {
////            log.trace("Resource not found {}", ex.getMessage());
////            List<ConstraintsViolationError> validationErrors = ex.getConstraintViolations()
////                    .stream()
////                    .map(violation -> new ConstraintsViolationError(violation.getPropertyPath().toString(),
////                            violation.getMessage()))
////                    .collect(Collectors.toList());
////            return ofType(request, HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), validationErrors);
////        }
//
//        @ExceptionHandler(MaxUploadSizeExceededException.class)
//        public final ResponseEntity<Map<String, Object>> handle(MaxUploadSizeExceededException ex,
//                WebRequest request) {
//            log.trace("Resource not found : {}", ex);
//            return ofType(request, HttpStatus.PAYLOAD_TOO_LARGE, ex);
//        }
//
//        @ExceptionHandler(MethodArgumentTypeMismatchException.class)
//        public final ResponseEntity<Map<String, Object>> handle(MethodArgumentTypeMismatchException ex,
//                WebRequest request) {
//            log.trace("Method arguments are not valid : {}", ex.getMessage());
//            return ofType(request, HttpStatus.BAD_REQUEST, ex);
//        }
//
//        @ExceptionHandler(MismatchedInputException.class)
//        public final ResponseEntity<Map<String, Object>> handle(MismatchedInputException ex,
//                WebRequest request) {
//            log.trace("Mismatched inout : {}", ex.getMessage());
//            return ofType(request, HttpStatus.BAD_REQUEST, ex);
//        }
//
//        @ExceptionHandler(BindException.class)
//        public final ResponseEntity<Map<String, Object>> handle(
//                BindException ex,
//                WebRequest request) {
//            List<ConstraintsViolationError> validationErrors = ex.getBindingResult()
//                    .getFieldErrors()
//                    .stream()
//                    .map(error -> new ConstraintsViolationError(error.getField(), error.getDefaultMessage()))
//                    .collect(Collectors.toList());
//
//            return ofType(request, HttpStatus.BAD_REQUEST,"Exception", validationErrors);
//        }
//
//        @ExceptionHandler(MethodArgumentNotValidException.class)
//        public final ResponseEntity<Map<String, Object>> handle(
//                MethodArgumentNotValidException ex,
//                WebRequest request) {
//            List<ConstraintsViolationError> validationErrors = ex.getBindingResult()
//                    .getFieldErrors()
//                    .stream()
//                    .map(error -> new ConstraintsViolationError(error.getField(), error.getDefaultMessage()))
//                    .collect(Collectors.toList());
//
//            return ofType(request, HttpStatus.BAD_REQUEST, ErrorCodes.INTERNAL_SERVER_ERROR.message, validationErrors);
//        }
//
//        @ExceptionHandler(Exception.class)
//        public ResponseEntity<Map<String, Object>> handle(Exception ex,
//                WebRequest request) {
//            log.error("Server failure", ex);
//            return ofType(request, HttpStatus.INTERNAL_SERVER_ERROR, ErrorCodes.INTERNAL_SERVER_ERROR.message);
//        }
//
//        protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String ex) {
//            return ofType(request, status, ex, Collections.EMPTY_LIST);
//        }
//
//
//
//        protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, Exception ex) {
//            return ofType(request, status, ex.getLocalizedMessage(), Collections.EMPTY_LIST);
//        }
//
//        private ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message,
//                List validationErrors) {
//        DefaultErrorAttributes defaultErrorAttributes=new DefaultErrorAttributes();
//            Map<String, Object> attributes = defaultErrorAttributes.getErrorAttributes(request,ErrorAttributeOptions.defaults());
//            attributes.put(STATUS, status.value());
//            attributes.put(ERROR, "getLocalizedReasonPhrase(status)");
//            attributes.put(MESSAGE, message);
//            attributes.put(ERRORS, validationErrors);
//            attributes.put(PATH, ((ServletWebRequest) request).getRequest().getRequestURI());
//            return new ResponseEntity<>(attributes, status);
//
//
//
//
//}

}
