package com.youtube.ecommerce.error;


public class ApplicationException extends RuntimeException {
    private final ErrorCodes errorCodes;
    public ApplicationException(ErrorCodes errorCodes) {

        this.errorCodes = errorCodes;
    }

}
