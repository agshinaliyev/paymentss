package com.youtube.ecommerce.error;

public interface Errors {

    String key();
    String message();


}
