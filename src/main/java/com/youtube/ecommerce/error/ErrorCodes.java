package com.youtube.ecommerce.error;

import lombok.RequiredArgsConstructor;


public enum ErrorCodes implements Errors {

    USERNAME_ALREADY_EXISTS("REGISTERING_HAS_FAILED" ,"This username has already been registered"),
    MAIL_ALREADY_EXISTS("REGISTERING_HAS_FAILED","This mail has already been registered"),
    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR","Internal Server Error"),
    PASSWORD_VIOLATION_ERROR("PASSWORD_SIZE_ERROR","Password size must be between 5 and 20");

     String key;
     String message;
    ErrorCodes(String key,String message){

        this.key =key;
        this.message=message;

    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public String message() {
        return message;
    }
}
