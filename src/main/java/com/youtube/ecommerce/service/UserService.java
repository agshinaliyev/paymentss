package com.youtube.ecommerce.service;

import com.youtube.ecommerce.dto.UserRequest;
import com.youtube.ecommerce.dto.UserResponse;
import com.youtube.ecommerce.error.ApplicationException;
import com.youtube.ecommerce.model.Authority;
import com.youtube.ecommerce.model.Roles;
import com.youtube.ecommerce.model.User;
import com.youtube.ecommerce.repository.AuthorityRepository;
import com.youtube.ecommerce.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.management.relation.Role;

import java.util.List;

import static com.youtube.ecommerce.error.ErrorCodes.*;

@Service
@RequiredArgsConstructor
public class UserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder encoder;

    public UserResponse registerUser(UserRequest request) {
        User user = modelMapper.map(request, User.class);
        if (userRepository.findByMailIgnoreCase(user.getMail()).isPresent())
            throw new ApplicationException(MAIL_ALREADY_EXISTS);
        if (userRepository.findByUsernameIgnoreCase(user.getUsername()).isPresent()) {
            throw new ApplicationException(USERNAME_ALREADY_EXISTS);
        }
        Authority authority =new Authority();
            authority.setRoles(Roles.USER);
        user.setAuthorities(List.of(authority));
        authorityRepository.save(authority);

        user.setPassword(encoder.encode(user.getPassword()));
        if (user.getPassword().length() < 5){
            throw new ApplicationException(PASSWORD_VIOLATION_ERROR);
        }

        return modelMapper.map(userRepository.save(user),UserResponse.class);

    }


}
