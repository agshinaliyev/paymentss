package com.youtube.ecommerce.service;

import com.youtube.ecommerce.dto.AccessToken;
import com.youtube.ecommerce.dto.LoginDto;
import com.youtube.ecommerce.security.JwtService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Data
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;

    @Override
    public AccessToken login(LoginDto body) {

        UsernamePasswordAuthenticationToken upa = new UsernamePasswordAuthenticationToken(body.getUsername(), body.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(upa);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String accessToken = jwtService.issueToken(authentication);
        AccessToken token = AccessToken.builder().accessToken(accessToken).build();


        return token;
    }
}
