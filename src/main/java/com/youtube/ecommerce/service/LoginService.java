package com.youtube.ecommerce.service;

import com.youtube.ecommerce.dto.AccessToken;
import com.youtube.ecommerce.dto.LoginDto;

public interface LoginService {

    AccessToken login(LoginDto loginDto);
}
