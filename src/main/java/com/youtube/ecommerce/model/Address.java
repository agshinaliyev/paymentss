package com.youtube.ecommerce.model;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.query.Order;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String country;
    private Long postCode;
    private Boolean isActive;
    @ManyToOne
    User user;

    @OneToMany
    List<Orders> orders=new ArrayList<>();





}
