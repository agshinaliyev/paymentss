package com.youtube.ecommerce.model;

public enum Gender {

    MALE,
    FEMALE,
    NONE
}
