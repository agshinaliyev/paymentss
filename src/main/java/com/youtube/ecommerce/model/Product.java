package com.youtube.ecommerce.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String shortDescription;
    private String longDescription;
    private Double price;
    @OneToOne(mappedBy = "product",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    Inventory inventory;
    @OneToMany
    List<OrdersQuantities> ordersQuantities = new ArrayList<>();



}
