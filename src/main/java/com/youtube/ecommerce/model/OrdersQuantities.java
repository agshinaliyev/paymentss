package com.youtube.ecommerce.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class OrdersQuantities {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer quantities;
    @ManyToOne
    Product product;
    @ManyToOne
    Orders order;



}
