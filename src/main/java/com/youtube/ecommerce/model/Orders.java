package com.youtube.ecommerce.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    User user;
    @ManyToOne
     Address address;
    @OneToMany(mappedBy = "order", cascade = CascadeType.REMOVE)
    List<OrdersQuantities> ordersQuantities = new ArrayList<>();


}
