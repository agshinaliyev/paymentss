package com.youtube.ecommerce.repository;

import com.youtube.ecommerce.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {
}
