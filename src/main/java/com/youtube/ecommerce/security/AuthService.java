package com.youtube.ecommerce.security;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface AuthService {

    Optional<Authentication> getAuthentication(HttpServletRequest request);
    boolean isBearerAuth(String header);
    Authentication getAuthenticationBearer(Claims claims);
    Optional<Authentication> getAuthenticationBearer(String header);
}
