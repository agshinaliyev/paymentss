package com.youtube.ecommerce.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.ParserBuilder;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.awt.*;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtService {
    @Value("${spring.security.secretKey}")
    private static Key key;
    @Value("${spring.security.expDate}")
    private static Long expDate;


    public Claims parseToken(String token){

        return Jwts.parser()
                .decryptWith((SecretKey) key)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }


    public String issueToken(Authentication authentication) {


        return Jwts.builder()
                .header().add(Map.of("type", "Jwt")).and()
                .subject(authentication.getName())
                .signWith(key)
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plusSeconds(expDate)))
                .claims(Map.of("role", authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)))
                .compact();


    }


}
