package com.youtube.ecommerce.controller;

import com.youtube.ecommerce.dto.AccessToken;
import com.youtube.ecommerce.dto.LoginDto;
import com.youtube.ecommerce.dto.UserRequest;
import com.youtube.ecommerce.dto.UserResponse;
import com.youtube.ecommerce.service.LoginService;
import com.youtube.ecommerce.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Validated
public class AuthenticationController {
    private final UserService userService;
    private final LoginService loginService;

    @PostMapping("/register")
    public UserResponse register(@Valid @RequestBody UserRequest registrationBody) {
        return userService.registerUser(registrationBody);

    }
    @PostMapping("/sign-in")
    public ResponseEntity<AccessToken> signIn(@Valid @RequestBody  LoginDto body){

        return ResponseEntity.ok(loginService.login(body));

    }


}
